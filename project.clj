(defproject suffolk "0.1.0-SNAPSHOT"

  :description "FIXME: write description"

  :url "http://example.com/FIXME"

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.229"]
                 [org.clojure/core.async "0.2.391"]
                 [rum "0.10.6"]]

  :plugins [[lein-cljsbuild "1.1.4"]]

  :profiles {:dev {:dependencies [[com.cemerick/piggieback "0.2.1"]
                                  [figwheel-sidecar "0.5.7"]
                                  [binaryage/devtools "0.8.2"]]}}

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src"]
                        :figwheel {:on-jsload "suffolk.core/on-js-reload"}
                        :compiler {:main suffolk.core
                                   :optimizations :none
                                   :output-to "resources/public/js/suffolk.js"
                                   :output-dir "resources/public/js/compiled/out"
                                   :asset-path "js/compiled/out"
                                   :verbose true
                                   :source-map true
                                   :source-map-timestamp true
                                   :cache-analysis true
                                   :parallel-build true
                                   :preloads [devtools.preload]}}]}

  )
