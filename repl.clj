
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Types

;; symbol
'symbol

;; keyword
:a

;; namespace keyword
::a

;; number
123

;; string
"ASDF"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Data Structures

;; list
(type '(1 2 3))

;; vector
(type [1 2 3])

;; hash map

{:a 1 :b 2}


(type {:a 1 :b 2})

;; set
(type #{:a :b :c})


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; immutable
(def a [1 2 3])

a

(conj a 4)

a

(=
 {:a 1 :b 2}
 (assoc {} :a 1 :b 2))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sequence abstraction

(seq [1 2 3])

(type (seq [1 2 3]))

(first [1 2 3])

(rest [1 2 3])

(range 10)

(map (fn [x] (+ x 1)) (range 10))

(map inc (range 10))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; maps

(def data {:a 1})

data

(get data :a)

(:a data)

(def data nil)


(assoc data :b 2)

data

(let [new (assoc data :b 2)]
  new)

data


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; atoms

(def data (atom {:a 1}))

data

(deref data)

@data

(assoc @data :b 2)

@data




(swap! data assoc :b 2)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; assoc-in update-in
(def data {:suffolk {:devs {:cljs   ['keeds]
                            :ruby   ['anders]
                            :js     ['sean 'steveb]
                            :golang ['elliot]}}})

data

(assoc-in data [:suffolk :devs :haskell] [])

(defn switch-all-kw
  [data path kw]
  (update-in data path
             (fn [d]
               (let [x (dissoc d kw)]
                 (merge {kw (vec (flatten (into (kw d) (vals x))))}
                        (zipmap (keys x) (map empty (vals x))))))))

(switch-all-kw data [:suffolk :devs] :cljs)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; functions

(fn [x] x)

((fn [x] x) 1)

(def id' (fn [x] x))

(id' 1)

(defn id'
  "return value (identity)"
  [x]
  x)

(id' 1)



(defn adder
  [x y]
  (+ x y))

(adder 1 2)





(defn adder
  [x y z]
  (+ (+ x y) z))

(adder 1 2 3)

(adder 1 2)





(defn adder
  [& args]
  (apply + args))

(adder 1 2 3 4)

(adder 1 2 3)

(adder 1 2)

(adder 1)




(defn adder
  ([x]
   x)
  ([x y]
   (+ x y))
  ([x y z]
   (+ x y z))
  ([x y z & args]
   (+ (adder x y z)
      (apply adder args))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; threading
(reduce + (map inc (filter even? (range 10))))

(->> (range 10)
     (filter even?)
     (map inc)
     (reduce +))

(-> [1 2 3]
    (conj 4 5 6)
    (conj :a :b :c)
    count)
