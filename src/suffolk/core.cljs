(ns suffolk.core
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:require
   [cljs.core.async :refer [chan put! <!]]
   [goog.events :as events]
   [rum.core :as rum]))


(defonce data (atom []))


(rum/defc mouse-pos [msg]
  [:div msg])


(rum/defc table < rum/reactive [data]
  [:ul
   (for [x (rum/react data)]
     [:li
      {:key (rand-int 1000000)}
      (str "Item: " x)
      [:button
       {:onClick (fn [_]
                   (swap! data (fn [xs] (remove #(= % x) xs))))}
       "X"]])])


(rum/defc main [data]
  [:div
   (table data)
   [:div
    [:button
     {:onClick (fn [_]
                 (swap! data concat (range 5)))}
     "Add List"]
    [:button
     {:onClick (fn [_]
                 (swap! data empty))}
     "Clear"]]])


(defn init [data]
  (rum/mount (main data)
             (.getElementById js/document "app")))


(defn on-js-reload
  []
  (init data))


(defn listen [el type]
  (let [c (chan 1)]
    (events/listen el type #(put! c %))
    c))

(let [c (listen (.-body js/document) goog.events.EventType.MOUSEMOVE)]
  (go
    (while true
      (let [x (<! c)]
        (rum/mount (mouse-pos (str (.-screenX x) ", " (.-screenY x)))
                   (.getElementById js/document "mouse"))))))


(init data)



(comment

  (map inc (range 5))

  (.log js/console (map inc (range 5)))

  (apply + (map inc (filter even? (range 5)))) 

  (->> (range 10)
       (filter even?)
       (map inc)
       (apply +))

  (swap! data concat (range 5))

  (swap! data rest)

  (swap! data empty)

  (swap! data (fn [xs] (remove #(= % 3) xs)))
  
  (defn even-inc
    [xs]
    (->> xs
         (filter even?)
         (map inc)))
  
  (swap! data even-inc)

  @data
  


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; JS Interop / Interact
  
  (let [c (.createClass js/React
                        (clj->js {:render (fn []
                                            (this-as this
                                              (let [props (js->clj (.-props this)
                                                                   :keywordize-keys true)]
                                                (.log js/console props)
                                                (.createElement js/React
                                                                "div"
                                                                #js {}
                                                                (str "Hello "
                                                                     (:who props)
                                                                     "!")))))}))
        f (.createFactory js/React c)]
    (.render js/ReactDOM
             (f #js {:who "Suffolk Devs"})
             (.getElementById js/document "app")))

  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Rum version
  
  (rum/defc hello [who]
    [:div (str "Hello " who "!")])
  
  (rum/mount (hello "Suffolk Developers")
             (.getElementById js/document "app"))
  
  )
